package com.example.safe;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
    String password = "";
    String pass_correct;
    EditText edt_pass;
    EditText input_pass;
    TextView txt_one, txt_two, txt_three, txt_four, txt_five, txt_six, txt_seven, txt_eight, txt_nine, txt_zero,  txt_ok;
    ImageView img_delete;
    TextView txt_forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        Paper.init(MainActivity.this);
        //view
        edt_pass = findViewById(R.id.edt_pass);
        txt_zero = findViewById(R.id.txt_zero);
        txt_one = findViewById(R.id.txt_one);
        txt_two = findViewById(R.id.txt_two);
        txt_three = findViewById(R.id.txt_three);
        txt_four = findViewById(R.id.txt_four);
        txt_five = findViewById(R.id.txt_five);
        txt_six = findViewById(R.id.txt_six);
        txt_seven = findViewById(R.id.txt_seven);
        txt_eight = findViewById(R.id.txt_eight);
        txt_nine = findViewById(R.id.txt_nine);
        txt_ok = findViewById(R.id.txt_ok);
        img_delete = findViewById(R.id.img_delete);
        txt_forgot = findViewById(R.id.txt_forgot);

        //event
        inputPass();
        forgotPassword();

    }

    private void forgotPassword() {
        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Forgot password");
                intent.putExtra(Intent.EXTRA_TEXT, pass_correct);
                intent.setData(Uri.parse("mailto:"+input_pass.getText().toString().trim())); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);
            }
        });
    }

    private void inputPass() {
        txt_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "0");
                edt_pass.setText(password);
                Toast.makeText(MainActivity.this, "aaa", Toast.LENGTH_SHORT).show();
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "1");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "2");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "3");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "4");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "5");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "6");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "7");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6)
                    txt_ok.setVisibility(View.VISIBLE);
            }
        });
        txt_eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "8");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6) {
                    txt_ok.setVisibility(View.VISIBLE);
                }
            }
        });
        txt_nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = concat(password, "9");
                edt_pass.setText(password);
                if (password.length() > 0)
                    img_delete.setVisibility(View.VISIBLE);
                if (password.length() >= 6) {
                    txt_ok.setVisibility(View.VISIBLE);
                }
            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pass_correct = Paper.book().read("pass");
                if (edt_pass.getText().toString().length() >= 6 && pass_correct==null)
                    dialogSignin();
                if (edt_pass.getText().toString().equals(pass_correct) && pass_correct!=null) {
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = edt_pass.getText().toString();
                edt_pass.setText(text.substring(0, text.length() - 1));
                password = edt_pass.getText().toString();
                if (password.length() < 6)
                    txt_ok.setVisibility(View.INVISIBLE);
                if (password.length() <= 0)
                    img_delete.setVisibility(View.INVISIBLE);

            }
        });
    }

    private void dialogSignin() {
        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).create();
        View view=LayoutInflater.from(this).inflate(R.layout.dialog_enter_email,null);
        input_pass = view.findViewById(R.id.edt_email_recall);
        Button btn_ok=view.findViewById(R.id.btn_ok_recall);
        Button btn_cancel=view.findViewById(R.id.btn_cancel_recall);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Paper.book().write("pass", password);
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Register unsuccessful!", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.setCancelable(true);
        dialog.setView(view);
        dialog.show();
    }

    public static String concat(String s1, String s2) {
        return new StringBuilder(s1).append(s2).toString();
    }
}
