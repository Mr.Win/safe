package com.example.safe;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class DetailActivity extends AppCompatActivity {
    EditText edt_first_name, edt_middle_name, edt_last_name, edt_phone, edt_company, edt_address, edt_mail,edt_date;
    Spinner spinner_phone,spinner_date, spinner_email;
    int DIALOG_ID = 0,DIALOG_ID_ADD=1;
    int day, month, year;
    int day_select,month_select,year_select;
    String new_field;
    LinearLayout container_phone,container_email,container_date,btn_add_new_field,container_add_new_info;
    Toolbar toolbar;
    List<String> list_spinner_phone=new ArrayList<>();
    List<String> list_spinner_email=new ArrayList<>();
    List<String> list_spinner_date=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //view
        edt_first_name = findViewById(R.id.edt_first_name);
        edt_middle_name = findViewById(R.id.edt_middle_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_phone = findViewById(R.id.edt_phone);
        edt_company = findViewById(R.id.edt_company);
        edt_address = findViewById(R.id.edt_address);
        edt_mail = findViewById(R.id.edt_email);
        edt_date = findViewById(R.id.edt_date);
        spinner_email = findViewById(R.id.spinner_email);
        spinner_phone=findViewById(R.id.spinner_phone);
        spinner_date = findViewById(R.id.spinner_date);
        container_phone = findViewById(R.id.container_phone);
        container_email = findViewById(R.id.container_email);
        container_date = findViewById(R.id.container_date);
        container_add_new_info=findViewById(R.id.container_add_new_info);
        btn_add_new_field=findViewById(R.id.btn_add_new_info);
        toolbar=findViewById(R.id.toolbar);


        //generalData
        generateData();

        //set spinner
        spinner_phone.setAdapter(new SpinnerAdapter(this,list_spinner_phone));
        spinner_email.setAdapter(new SpinnerAdapter(this,list_spinner_email));
        spinner_date.setAdapter(new SpinnerAdapter(this,list_spinner_date));

        //toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        //set current day
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        //event
        edt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DIALOG_ID);

            }
        });
        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_phone.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View view, int i, KeyEvent keyEvent) {
                        if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                                (i == KeyEvent.KEYCODE_ENTER)){
                            addNewPhoneView();
                            return true;
                        }
                        return false;
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt_mail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                edt_mail.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View view, int i, KeyEvent keyEvent) {
                        if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                                (i == KeyEvent.KEYCODE_ENTER)){
                            addNewEmailView();
                            return true;
                        }
                        return false;
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               if (charSequence.length() >0)
                   addNewDateView();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        btn_add_new_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDialogAddNewField();
            }
        });
    }

    private void generateData() {
        list_spinner_phone.addAll(Arrays.asList("Mobile", "Work", "Home", "Main", "Work Fax", "Home Fax", "Paper", "Other", "Custom"));
        list_spinner_email.addAll(Arrays.asList("Work", "Home", "Other", "Custom"));
        list_spinner_date.addAll(Arrays.asList("Birthday", "Anniversary", "Other", "Custom"));
    }

    private void startDialogAddNewField() {
        final AlertDialog alertDialog=new AlertDialog.Builder(DetailActivity.this).create();
        View view=LayoutInflater.from(DetailActivity.this).inflate(R.layout.dialog_add_new_field_layout,null);
        final EditText edt_name_field=view.findViewById(R.id.edt_name_field);
        Button btn_ok=view.findViewById(R.id.btn_ok);
        Button btn_cancel=view.findViewById(R.id.btn_cancel);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do
                if (edt_name_field.getText().toString().isEmpty()){
                    Toast.makeText(DetailActivity.this, "Please fill new field", Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    new_field=edt_name_field.getText().toString();
                    alertDialog.dismiss();
                    addNewField();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.setView(view);
        alertDialog.show();
    }

    private void addNewField() {
        LayoutInflater layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.item_contact_new_info_layout, null);
        TextView txt_add_new_info =  addView.findViewById(R.id.txt_add_new_info);
        EditText edt_add_new = (EditText) addView.findViewById(R.id.edt_add_new);
        ImageView img_add_new = addView.findViewById(R.id.img_add_view);
        txt_add_new_info.setText(new_field);

        final View.OnClickListener thisListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) addView.getParent()).removeView(addView);
            }
        };
        img_add_new.setOnClickListener(thisListener);
        container_add_new_info.addView(addView);
    }

    private void addNewDateView() {
        LayoutInflater layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.item_contact_pick_layout, null);
        Spinner spinner_add =  addView.findViewById(R.id.sppiner_add_new);
        final EditText edt_date_add =  addView.findViewById(R.id.edt_date);
        ImageView img_add_new = addView.findViewById(R.id.img_add_view);
        spinner_add.setAdapter(new SpinnerAdapter(this,list_spinner_date));
        edt_date_add.setText(edt_date.getText().toString());
        img_add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LinearLayout)addView.getParent()).removeView(addView);
            }
        });
        edt_date_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DIALOG_ID_ADD);
                edt_date_add.setText(String.valueOf(day_select)+"/"+String.valueOf(month_select)+"/"+String.valueOf(year_select));
            }
        });
        container_date.addView(addView);
        edt_date.getText().clear();
    }

    private void addNewEmailView() {
        LayoutInflater layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.item_contact_edit_layout, null);
        Spinner spinner_add =  addView.findViewById(R.id.sppiner_add_new);
        EditText edt_add_new = addView.findViewById(R.id.edt_add_new);
        ImageView img_add_new = addView.findViewById(R.id.img_add_view);
        spinner_add.setAdapter(new SpinnerAdapter(DetailActivity.this,list_spinner_email));
        edt_add_new.setText(edt_mail.getText().toString());

        final View.OnClickListener thisListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) addView.getParent()).removeView(addView);
            }
        };
        edt_add_new.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==0)
                    ((LinearLayout) addView.getParent()).removeView(addView);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        img_add_new.setOnClickListener(thisListener);
        container_email.addView(addView);
        edt_mail.getText().clear();
    }

    private void addNewPhoneView() {
        LayoutInflater layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.item_contact_edit_layout, null);
        Spinner spinner_add =  addView.findViewById(R.id.sppiner_add_new);
        EditText edt_add_new =  addView.findViewById(R.id.edt_add_new);
        ImageView img_add_new =  addView.findViewById(R.id.img_add_view);
        spinner_add.setAdapter(new SpinnerAdapter(DetailActivity.this,list_spinner_phone));
        edt_add_new.setText(edt_phone.getText().toString());

        final View.OnClickListener thisListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) addView.getParent()).removeView(addView);
            }
        };
        edt_add_new.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==0)
                    ((LinearLayout) addView.getParent()).removeView(addView);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        img_add_new.setOnClickListener(thisListener);
        container_phone.addView(addView);
        edt_phone.getText().clear();
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID)
            return new DatePickerDialog(this, datePickerListener, year, month, day);
        if (id == DIALOG_ID_ADD)
            return new DatePickerDialog(this, datePickerListener_add, year, month, day);
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;
            //get date
            day_select=selectedDay;
            month_select=selectedMonth+1;
            year_select=selectedYear;
            edt_date.setText(String.valueOf(selectedDay)+"/"+String.valueOf(selectedMonth+1)+"/"+String.valueOf(selectedYear));
        }
    };
    private DatePickerDialog.OnDateSetListener datePickerListener_add = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;
            //get date
            day_select=selectedDay;
            month_select=selectedMonth+1;
            year_select=selectedYear;
        }
    };

//    private void listAllAddView() {
//        int childCount = container.getChildCount();
//        for(int i=0; i<childCount; i++){
//            View thisChild = container.getChildAt(i);
//
//        }
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.menu_save){

        }
        return super.onOptionsItemSelected(item);
    }
}