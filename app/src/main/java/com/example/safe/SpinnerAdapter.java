package com.example.safe;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    List<String> listData;
    LayoutInflater layoutInflater;

    public SpinnerAdapter(Context context, List<String> listData) {
        this.context = context;
        this.listData = listData;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view==null)
            view=layoutInflater.inflate(R.layout.spinner_item,null);
        TextView txt_spinner_item=view.findViewById(R.id.txt_spinner_item);
        txt_spinner_item.setText(listData.get(i));
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view=super.getDropDownView(position,convertView,parent);
        LinearLayout ln=(LinearLayout)view;
        TextView tv=(TextView)ln.findViewById(R.id.txt_spinner_item);
        tv.setGravity(Gravity.LEFT);
        tv.setTextColor(Color.BLACK);
        tv.setPadding(20,20,20,20);
        tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return view;
    }
}
